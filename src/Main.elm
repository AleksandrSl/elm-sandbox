import Browser
import Browser.Navigation as Nav
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Url
import Url.Builder
import Http exposing (expectStringResponse, Error)
import Base64
import Debug
import Routes


main : Program () Model Msg
main =
  Browser.application
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    , onUrlChange = UrlChanged
    , onUrlRequest = LinkClicked
    }

type Model =
  Normal LoginPage
  | NotFound

type alias LoginPage =
  { key : Nav.Key
  , name: String
  , password: String
  , route: Routes.Route
  }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
   case Routes.parse url of
        Just r -> ( Normal ( LoginPage key "a" "b" r), Cmd.none )
        Nothing -> ( NotFound, Cmd.none )

type Msg
  = LinkClicked Browser.UrlRequest
  | UrlChanged Url.Url
  | NameChanged String
  | PasswordChanged String
  | Login
  | Demo
  | LoginStatus (Result Http.Error ())
  | FakeLogin


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case model of
    NotFound -> ( NotFound, Cmd.none)
    Normal m->
        case msg of

            LinkClicked urlRequest ->
              case urlRequest of
                Browser.Internal url ->
                  ( model
                  , Nav.pushUrl m.key (Url.toString url)
                  )

                Browser.External href ->
                  ( model
                  , Nav.load href
                  )

            UrlChanged url ->
              ( case Routes.parse url of
                        Just r -> ( Normal { m | route = r}, Cmd.none )
                        Nothing -> ( NotFound, Cmd.none )
              )
            NameChanged name ->
             ( Normal { m | name = name }
             , Cmd.none
             )
            PasswordChanged password ->
             ( Normal { m | password = password }
             , Cmd.none
             )

            Login ->
                ( Normal m,
                  tryLogin m.name m.password
                )

            Demo -> ( model, tryDemo )

            LoginStatus result ->
                ( model,
                  case result of
                      Ok () -> let _ = Debug.log "Logged in successful" "" in Cmd.none
                      Err error ->
                        case error of
                          Http.BadStatus response -> let _ = Debug.log "response: " response.body in Cmd.none
                          _ -> Cmd.none
                )

            FakeLogin ->
                ( model,
                  Nav.pushUrl m.key "/home"
                )


subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.none


tryLogin: String -> String -> Cmd Msg
tryLogin name password =
  Http.send LoginStatus (Http.request
                           { method = "GET"
                             , headers = [
                                 Http.header "Authorization" ("Basic " ++ encodedLoginInfo "user" "password")
                             ]
                             , url = "http://localhost:8080/login"
                             , body = Http.emptyBody
                             , expect = expectStringResponse (\_ -> Ok ())
                             , timeout = Nothing
                             , withCredentials = True
                             }
                        )

tryDemo =
    Http.send LoginStatus (Http.request
                            {
                              method = "GET"
                              , headers = []
                              , url = "http://localhost:8080/demo"
                              , body = Http.emptyBody
                              , expect = expectStringResponse (\_ -> Ok ())
                              , timeout = Nothing
                              , withCredentials = True
                            }
    )


encodedLoginInfo name password = Base64.encode (name ++ ":" ++ password)


-- VIEW
view : Model -> Browser.Document Msg
view model =
  { title = "URL Interceptor"
  , body =
    case model of
        Normal m ->
            case m.route of
              Routes.LoginRoute ->
                  [ div [] [ input [ type_ "text", placeholder "name", value m.name, onInput NameChanged ] []
                  ]
                  , div [] [ input [ type_ "password", placeholder "", value m.password, onInput PasswordChanged ] []
                  ]
                  , button [ onClick Login ] [ text "Login" ]
                  , button [ onClick Demo ] [ text "Demo request" ]
                  , button [ onClick FakeLogin ] [ text "Fake login" ]
                  ]
              Routes.HomeRoute ->
                [ div [] [ text "You are logged in successfully"]  ]
        NotFound ->
          [ div [] [ h1 [] [ text "Not Found"]]  ]
  }

module Routes exposing (Route(..), parse)

import Browser.Navigation as Nav
import Url
import Url.Builder
import Url.Parser exposing ((</>), Parser, map, oneOf, s)

type Route =
    LoginRoute
    | HomeRoute


parseUrl: Parser (Route -> a) a
parseUrl =
    oneOf
        [ map LoginRoute loginParser
        , map HomeRoute homeParser
        ]

loginParser = s "login"
homeParser = s "home"

loginBuilder = Url.Builder.absolute [ "login" ] []
homeBuilder = Url.Builder.absolute [ "home" ] []

parse: Url.Url -> Maybe Route
parse url = Url.Parser.parse parseUrl url

routeToUrl: Route -> String
routeToUrl route =
  case route of
    LoginRoute -> loginBuilder
    HomeRoute -> homeBuilder